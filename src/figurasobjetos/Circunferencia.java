/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figurasobjetos;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */

public class Circunferencia extends Figura{
    private int radio;
    private float diametro;
    private float area;
    
    static final float PI = 3.14159f;
    
    public Circunferencia(int radius){
        radio = radius;
        area = PI * this.radio * this.radio;
        diametro = 2 * PI * this.radio;
    }
    public float getDiametro(){
        return this.diametro;
    }
    public float getArea(){
        return area;
    }
    public int getRadio(){
        return this.radio;
    }
    public Circunferencia aumentarRadio(){
        radio++;
        return this;
    }
}
