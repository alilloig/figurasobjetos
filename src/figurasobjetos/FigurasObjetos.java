/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figurasobjetos;

import java.util.Scanner;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class FigurasObjetos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Introduzca el radio de la circunferencia:");
        Circunferencia circ = new Circunferencia(input.nextInt());
        System.out.println("Introduzca el color de la circunferencia:");
        circ.setColor(input.next());
        System.out.println("Es una circunferencia de color "+circ.getColor()+" de radio "+
                circ.getRadio()+", diametro "+circ.getDiametro()+" y area "+circ.getArea());
    }
    
}
