/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figurasobjetos;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class Figura {
    private String color;
    
    public void setColor(String newColor){
        this.color = newColor;
    }
    public String getColor(){
        return this.color;
    }
}
